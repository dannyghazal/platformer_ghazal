﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class loadScene : MonoBehaviour
{
    private int nextScene;

    private void start()
    {
        nextScene = SceneManager.GetActiveScene().buildIndex + 1;
    }

    public void SceneLoader(int SceneIndex)
    {
        SceneManager.LoadScene(SceneIndex);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
     if(other.CompareTag("player"))
        {
            SceneManager.LoadScene(nextScene);
        }
    }
    private void Update()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }
    
}
