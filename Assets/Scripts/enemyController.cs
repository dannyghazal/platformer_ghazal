﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyController : MonoBehaviour
{
    public LayerMask enemyMask;
    Rigidbody2D enemyBody;
    Transform enemyTrans;
    float enemyWidth;
    public float speed;
    [SerializeField] public float enemyDamage;
    [SerializeField] private GameManager lifeController;


    // Start is called before the first frame update
    void Start()
    {
        enemyTrans = this.transform;
        enemyBody = this.GetComponent<Rigidbody2D>();
        enemyWidth = this.GetComponent<SpriteRenderer>().bounds.extents.x;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 lineCastPos = enemyTrans.position - enemyTrans.right * enemyWidth;
        Debug.DrawLine(lineCastPos, lineCastPos + Vector2.down);
        bool isGrounded = Physics2D.Linecast(lineCastPos, lineCastPos + Vector2.down, enemyMask);

        if(isGrounded == false)
        {
            Vector3 currRot = enemyTrans.eulerAngles;
            currRot.y += 180;
            enemyTrans.eulerAngles = currRot;
        }

        Vector2 myVel = enemyBody.velocity;
        myVel.x = -enemyTrans.right.x * speed;
        enemyBody.velocity = myVel;
    }


    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "player")
        {
            //Destroy(GameObject.FindWithTag("player"));
            lifeController.lives = lifeController.lives - enemyDamage;
            lifeController.UpdateLives();
            this.gameObject.SetActive(false);
        }
    }

}
