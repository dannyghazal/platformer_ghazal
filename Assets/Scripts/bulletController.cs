﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletController : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Rigidbody2D rb;
    public float bulletSpeed = 20.0f;
    public int damage = 50;


    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right * bulletSpeed;
    }
    private void Update()
    {
        if(bulletPrefab == null)
        {
            Destroy(GameObject.FindGameObjectWithTag("bullet"));
        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "enemy")
        {
            Destroy(GameObject.FindWithTag("enemy"));
        }
    }
}
