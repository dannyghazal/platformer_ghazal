﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public Vector2 lastCheckPointPos;
    public float lives;
    [SerializeField] private Text livesText;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(lives < 1)
        {
            Destroy(GameObject.FindWithTag("player"));
           // SceneManager
        }
    }

    public void UpdateLives()
    {
        livesText.text = lives.ToString("0");
    }
}
